﻿


namespace UsersAPIService.Controllers
{
    using BusinessLayer.Interfaces;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Threading.Tasks;
    using log4net;
    using Microsoft.Extensions.Logging;
    using UsersAPIService.Models;
    using AutoMapper;
    using DataAccesLayer.Models;

    [Route("api/users")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersBusiness _userBusinessService;
        // private readonly ILogger _logger;

        public UsersController(IUsersBusiness userBusinessService/*, ILogger logger*/)
        {
            _userBusinessService = userBusinessService;
            //_logger = logger;
        }


        [HttpGet("all")]
        public async Task<IActionResult> GetAllUsers()
        {
            try
            {
                return Ok(await _userBusinessService.GetAllUsersAsync());
            }
            catch (Exception ex)
            {
                //_logger.LogError("error in UsersController -> GetAllUsers: ", ex);
                throw;
            }
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetUserById(int userId)
        {
            try
            {
                return Ok(await _userBusinessService.GetByIdAsync(userId));
            }
            catch(Exception ex)
            {
                //_logger.LogError("error in UsersController -> GetUserById: ", ex);
                throw;
            }
        }

        [HttpPut("add")]
        public void InsertUser(Users user)
        {
            try
            {
                _userBusinessService.InsertUser(user);
            }
            catch (Exception ex)
            {
                //Logger...
                throw;
            }
        }

        [HttpPut("update")]
        public void UpdateUser(Users user)
        {
            try
            {
                _userBusinessService.UpdateUser(user);
            }
            catch (Exception ex)
            {
                //Logger...
                throw;
            }
        }

    }
}
