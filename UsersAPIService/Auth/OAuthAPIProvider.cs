﻿using DataAccesLayer.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace UsersAPIService.Auth
{
    public class OAuthAPIProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            try
            {
                string userName = context.UserName;
                string pwd = context.Password;
                var scope = context.Scope;

                var user = await ValidateUser(userName, pwd);

                if (user != null)
                {
                    var claims = new List<Claim>();
                    var ticketPropierties = new Dictionary<string, string>();

                    claims.Add(new Claim("Id", user.Id.ToString()));
                    claims.Add(new Claim("Username", user.Username));

                    foreach (var item in user.UsersRoles)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, item.Role.RoleName));
                    }

                    ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
                    // ClaimsIdentity cookiesClaimIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationType);


                    identity.AddClaim(new Claim("scope", scope.FirstOrDefault()));
                    ticketPropierties.Add("scope", scope.FirstOrDefault().ToString());

                    // AuthenticationTicket ticket = new AuthenticationTicket(identity, new AuthenticationProperties(ticketPropierties));
                    // context.Validated(ticket);
                }
                else
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    context.Rejected();
                }
            }
            catch (Exception ex)
            {
                context.SetError("Server error, try again later");
                context.Rejected();
            }

        }

        private async Task<Users> ValidateUser(string user, string pwd)
        {
            //TODO: Search user with password
            return new Users();
        }
    }
}
