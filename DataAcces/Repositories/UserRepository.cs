﻿
namespace DataAccesLayer.Repositories
{
    using DataAccesLayer.Contexts;
    using DataAccesLayer.Models;
    using DataAccesLayer.Repository.Intefaces;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class UserRepository : IUserRepository
    {
        private readonly UsersDBContext _context;

        public UserRepository()
        {
            _context = new UsersDBContext();
        }

        public UserRepository(UsersDBContext context)
        {
            _context = context;
        }
        

        public async Task<IEnumerable<Users>> GetAllUsersAsync()
        {
            try
            {
                return await _context.Users.ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Users> GetByIdAsync(int userId)
        {
            try
            {
                return await _context.Users.Where(user => user.Id == userId).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Insert(Users user)
        {
            try
            {
                _context.AddAsync(user);
                Save();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void Update(Users user)
        {
            try
            {
                _context.Update(user);
                Save();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(Users user)
        {
            try
            {
                _context.Remove(user);
                Save();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public void Save()
        {
            _context.SaveChangesAsync();
        }
    }
}
