﻿using DataAccesLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccesLayer.Interfaces
{
    public interface IUsersData
    {
        Task<IEnumerable<Users>> GetAllUsersAsync();
        Task<Users> GetById(int userId);
        void Insert(Users user);
        void Update(Users user);
        void Delete(int EmployeeID);
    }
}
