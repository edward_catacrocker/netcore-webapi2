﻿

namespace UsersAPIService
{
    using Business;
    using BusinessLayer.Interfaces;
    using DataAcces;
    using DataAccesLayer.Contexts;
    using DataAccesLayer.Interfaces;
    using DataAccesLayer.Repositories;
    using DataAccesLayer.Repository.Intefaces;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            var connection = @"Server=(localdb)\mssqllocaldb;Database=UsersDB;Trusted_Connection=True;ConnectRetryCount=0";
            services.AddDbContext<UsersDBContext>(options => options.UseSqlServer(connection));

            //Register services...
            services.AddScoped<IUsersBusiness, UsersBusiness>();
            services.AddScoped<IUsersData, UsersData>();
            services.AddScoped<IUserRepository, UserRepository>();
            
        }



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            // loggerFactory.AddLog4Net();
            app.UseHttpsRedirection();
            app.UseMvc();
        }

       
    }
}
